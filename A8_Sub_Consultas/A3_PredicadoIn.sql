#Realizar una consulta que muestre el nombre y el precio de los 
#productos de los que se han pedido más de 20 unidades
SELECT
    productos.NOMBREARTÍCULO,
    productos.PRECIO
FROM
    productos
WHERE
    productos.CÓDIGOARTÍCULO IN(
    SELECT
        productos_pedidos.CÓDIGO_ARTÍCULO
    FROM
        productos_pedidos
    WHERE
        productos_pedidos.UNIDADES > 20
);
#
#En este caso se compara el campo códigoartículo con la lista que
#retorna la subconsulta preguntando si están en el listado
#
#Ejemplo utilizando INNER JOIN
SELECT
    productos.NOMBREARTÍCULO,
    productos.PRECIO
FROM
    productos
INNER JOIN 
	productos_pedidos 
	ON productos.CÓDIGOARTÍCULO = productos_pedidos.CÓDIGO_ARTÍCULO
WHERE
    productos_pedidos.UNIDADES > 20