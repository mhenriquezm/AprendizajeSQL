#Crear una consulta que devuelva los artículos cuyo precio sea 
#superior de todos los artículos de la sección de cerámica
SELECT
    *
FROM
    productos
WHERE
    productos.PRECIO > ALL(
    SELECT
        productos.PRECIO
    FROM
        productos
    WHERE
        SECCIÓN = "CERÁMICA"
)
#
#Al usar el operador ALL indicamos que el criterio, el precio en 
#este caso debe ser superior a todos los registros devueltos en la
#sub consulta