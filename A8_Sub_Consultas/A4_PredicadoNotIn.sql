#Realizar una consulta que muestre el aquellos clientes que no han
#pagado con tarjeta o no han hecho pedidos
SELECT
    clientes.EMPRESA,
    clientes.POBLACIÓN
FROM
    clientes
WHERE
    clientes.CÓDIGOCLIENTE NOT IN(
    SELECT
        pedidos.CÓDIGOCLIENTE
    FROM
        pedidos
    WHERE
        pedidos.FORMADEPAGO = "TARJETA"
)