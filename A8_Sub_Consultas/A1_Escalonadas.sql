#Realizar una consulta que muestre el nombre y la sección de los
#productos cuyo precio sea superior a la media
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN
FROM
    productos
WHERE
    productos.PRECIO >(
    SELECT
        ROUND(AVG(productos.PRECIO), 2) MEDIA_PRODUCTOS
    FROM
        productos
)
#
#Se realiza una sub consulta escalonada, dicha consulta establece
#una consulta padre y una hija, esta segunda retorna un único 
#registro que tiene una fila y una columna; este es usado como 
#criterio