# AprendizajeSQL
Aprender el lenguaje universal de acceso a datos SQL bajo el siguiente esquema: 
1. Características del lenguaje SQL. Utilidad del lenguaje. 
2. Comandos SQL. Grupos de comandos. 
3. Consultas de selección.     
   3.1 Consultas multitabla.
   3.2 Consultas de agrupación o resumen.
   3.3 Consultas de cálculo.
   3.4 Subconsultas.
4. Consultas de acción.
   4.1 Creación de tabla.
   4.2 Consultas de actualización.
   4.3 Consultas de eliminación.
   4.4 Consultas de datos anexados.
5. Consultas de referencias cruzadas.
6. Consultas de definición de datos.
   6.1 Tipos de datos.
   6.2 Índices.
   6.3 Integridad referencial.

Curso de pildorasinformaticas: https://www.youtube.com/watch?v=iOiyJgnN71c&list=PLU8oAlHdN5Bmx-LChV4K3MbHrpZKefNwn
