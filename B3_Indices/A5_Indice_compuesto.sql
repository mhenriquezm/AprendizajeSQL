#Crear una tabla llamada ejemplo con los campos dni, nombre, 
#apellido y edad, agregar un índice compuesto en los campos
#nombre y apellido
DROP TABLE IF EXISTS
    ejemplo;
CREATE TABLE ejemplo(
    dni VARCHAR(13),
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    edad TINYINT
); 
CREATE UNIQUE INDEX indice_compuesto ON
    ejemplo(nombre, apellido)
#
#Los índices compuestos son índices únicos, pero aplicados a 
#varios campos