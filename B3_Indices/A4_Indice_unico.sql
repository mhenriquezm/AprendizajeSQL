#Crear una tabla llamada ejemplo con los campos dni, nombre, 
#apellido y edad, agregar un índice único en el campo apellido
DROP TABLE IF EXISTS
    ejemplo;
CREATE TABLE ejemplo(
    dni VARCHAR(13),
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    edad TINYINT
); 
CREATE UNIQUE INDEX indice_unico ON
    ejemplo(apellido)
#
#Los índices únicos son parecidos a los ordinarios excepto porque
#no permiten valores duplicados, pero si nulos