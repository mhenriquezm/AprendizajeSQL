#Crear una tabla llamada ejemplo con los campos dni, nombre, 
#apellido y edad, los campos nombre y apellido deben ser 
#clave primaria
DROP TABLE IF EXISTS
    ejemplo;
CREATE TABLE ejemplo(
    dni VARCHAR(13),
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    edad TINYINT
); 
ALTER TABLE
    ejemplo ADD PRIMARY KEY(nombre, apellido)
#
#Se pueden tener varias PK o una clave primaria compuesta por 
#varios campos, es decir, la información en ambos campos no puede
#ser nula, ni puede repetirse, es decir, no se podrán almacenar
#dos personas con el mismo nombre y apellido, pueden tener el 
#mismo nombre o el mismo apellido, pero no ambos a la vez