#Crear una tabla llamada ejemplo con los campos dni, nombre, 
#apellido y edad, el campo dni debe ser clave primaria
DROP TABLE IF EXISTS
    ejemplo;
CREATE TABLE ejemplo(
    dni VARCHAR(13),
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    edad TINYINT
); 
ALTER TABLE
    ejemplo ADD PRIMARY KEY(dni)
#
#El índice PRIMARY KEY identifica cada registro como único, 
#no puede ser nulo y no se puede repetir