#Crear una tabla llamada ejemplo con los campos dni, nombre, 
#apellido y edad, agregar un índice ordinario en el campo apellido
DROP TABLE IF EXISTS
    ejemplo;
CREATE TABLE ejemplo(
    dni VARCHAR(13),
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    edad TINYINT
); 
CREATE INDEX indice_ordinario ON
    ejemplo(apellido)
#
#Los índices ordinarios son exactamente iguales que un campo 
#normal, pero permiten una mayor rapidez al momento de buscar o
#realizar consultas referentes a su campo asociado