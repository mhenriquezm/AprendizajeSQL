#Los procedimiendos como las funciones adquieren todo su potencial
#Al utilizar el paso de parámetros y funcionan exactamente igual
#Este procedure actualizará el precio del producto dado
CREATE PROCEDURE actualizar_productos(
    n_precio DOUBLE,
    id_articulo VARCHAR(4)
)
UPDATE
    productos
SET
    productos.PRECIO = n_precio
WHERE
    productos.CÓDIGOARTÍCULO = id_articulo
#
#En SQL primero se da nombre y luego el tipo de dato
#Llamamos al procedimiento pasando los parámetros igual que una 
#función convencional
CALL actualizar_productos(60, "AR22")