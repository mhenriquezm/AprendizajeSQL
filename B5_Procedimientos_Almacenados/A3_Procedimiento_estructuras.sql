#Crear un procedimiento que devuelva la edad en función del año 
#que recibe por parámetro
DELIMITER $$
CREATE PROCEDURE calcular_edad(fecha_nacimiento DATE)
BEGIN
    #Declaración de variables (entrada)
    DECLARE anio_actual INT DEFAULT YEAR(NOW());
    DECLARE edad INT;
    
    #Calcular edad (proceso)
    SET edad = anio_actual - YEAR(fecha_nacimiento);
    
    #Devolver resultado (salida)
    SELECT edad;
    #El select equivale al return
END;$$
DELIMITER ;
#
#El END finaliza con dos delimitadores, uno de bloque $$ que se
#indica al principio del documento el default ; al finalizar el
#procedure se formatea el delimitador al default
#
#En un procedimiento podemos utilizar las estructuras básicas de 
#la programación, si el procedimiento llevará varias líneas de 
#ejecución se utilizan las estructuras BEGIN (inicio) y END (fin)
#la sintaxis es similar a los lenguajes convencionales con ;