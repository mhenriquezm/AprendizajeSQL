-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2019 a las 16:37:17
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `curso_sql_procedimientos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_actualizaciones`
--

CREATE TABLE `auditoria_actualizaciones` (
  `codigo_articulo_anterior` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre_articulo_anterior` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seccion_articulo_anterior` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio_anterior` double(10,2) DEFAULT NULL,
  `importado_anterior` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais_origen_anterior` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_anterior` date DEFAULT NULL,
  `codigo_articulo_nuevo` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre_articulo_nuevo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seccion_articulo_nuevo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio_nuevo` double(10,2) DEFAULT NULL,
  `importado_nuevo` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais_origen_nuevo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nuevo` date DEFAULT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_modificado` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `auditoria_actualizaciones`
--

INSERT INTO `auditoria_actualizaciones` (`codigo_articulo_anterior`, `nombre_articulo_anterior`, `seccion_articulo_anterior`, `precio_anterior`, `importado_anterior`, `pais_origen_anterior`, `fecha_anterior`, `codigo_articulo_nuevo`, `nombre_articulo_nuevo`, `seccion_articulo_nuevo`, `precio_nuevo`, `importado_nuevo`, `pais_origen_nuevo`, `fecha_nuevo`, `usuario`, `fecha_modificado`) VALUES
('AR22', 'MARTILLO', 'FERRETERÍA', 11.40, 'FALSO', 'ESPAÑA', '2001-09-04', 'AR22', 'MARTILLO', 'FERRETERÍA', 60.00, 'FALSO', 'ESPAÑA', '2001-09-04', 'root@localhost', '2019-11-21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
