#Crear un procedimiento que muestre los clientes de madrid
CREATE PROCEDURE mostrar_clientes()
SELECT
    *
FROM
    clientes
WHERE
    clientes.POBLACIÓN = "MADRID"
#
#El procedimiento se crea con el comando CREATE PROCEDURE
#Se nombra como cualquier función o método y sus paréntesis
#Luego se programa la acción o acciones a ejecutar
#
#La forma de llamar a un procedimiento es igual que una función
CALL mostrar_clientes()
#
#El procedimiento se almacena en el servidor y es este mismo quien
#se encarga de realizar el trabajo cuando es llamado el 
#procedimiento, como cualquier función convencional no funciona 
#hasta que se llama. Esto encapsula la consulta evitando inyección
#sql y también impidiendo al usuario final tener contacto directo
#con la BBDD