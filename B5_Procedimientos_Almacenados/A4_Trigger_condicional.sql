#Crear un trigger que valide que el precio esté comprendido entre
#0 y 1000
DROP TRIGGER IF EXISTS productos_bu;
DELIMITER $$
CREATE TRIGGER validar_precios_bu BEFORE UPDATE
ON productos FOR EACH ROW
    BEGIN
        IF(NEW.PRECIO < 0) THEN
            SET NEW.PRECIO = OLD.PRECIO;
        ELSEIF(NEW.PRECIO > 1000) THEN
            SET NEW.PRECIO = OLD.PRECIO;
        END IF;
    END;$$
DELIMITER ;
#
#MySQL no soporta múltiples triggers para la misma acción