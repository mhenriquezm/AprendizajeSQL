#Realizar una consulta que muestre el nombre del artículo, sección,
#pero y un nuevo campo que muestre un descuento de 3 $ a cada 
#artículo
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO,
    (productos.PRECIO - 3) AS PRECIOACTUAL
FROM
    productos