#Realizar una consulta visualizando los campos NOMBRE ARTÍCULO, 
#SECCIÓN, PRECIO de la tabla PRODUCTOS y un campo nuevo que 
#nombramos con el texto PRECIO_AUMENTADO_EN_2. 
#Debe mostrar el PRECIO con un incremento de un 2% del PRECIO. 
#Sólo debemos tener en cuenta los registros de la sección 
#FERRETERÍA. El nuevo campo debe aparecer en Euros y con 2 
#lugares decimales.
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO,
    ROUND((productos.PRECIO * 1.02), 2) AS PRECIO_AUMENTADO_EN_2
FROM
    productos
WHERE
    productos.SECCIÓN = "FERRETERÍA"