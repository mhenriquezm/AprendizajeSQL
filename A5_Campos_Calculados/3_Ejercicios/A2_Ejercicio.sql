#Realizar una consulta visualizando los campos FECHA, SECCIÓN, 
#NOMBRE ARTÍCULO y PRECIO de la tabla PRODUCTOS y un campo nuevo 
#que nombramos con el texto DTO2 €_EN_CERÁMICA. 
#Debe mostrar el resultado de aplicar sobre el campo PRECIO la 
#resta de 2 € sólo a los artículos de la sección CERÁMICA. 
#El formato del nuevo campo debe aparecer con 2 lugares decimales. 
#Ordenar el resultado de la consulta por el campo FECHA descendente.
SELECT
    productos.FECHA,
    productos.SECCIÓN,
    productos.NOMBREARTÍCULO,
    productos.PRECIO,
    ROUND((productos.PRECIO - 2), 2) AS DTO2_€_EN_CERÁMICA
FROM
    productos
WHERE
    productos.SECCIÓN = "CERÁMICA"
ORDER BY
    productos.FECHA
DESC
    