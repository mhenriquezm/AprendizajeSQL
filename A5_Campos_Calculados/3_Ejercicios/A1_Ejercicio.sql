#Realizar una consulta en la que se visualicen los campos: 
#NOMBRE ARTÍCULO, SECCIÓN, PRECIO de la tabla PRODUCTOS 
#y un campo nuevo que nombramos con el texto DESCUENTO_7. 
#Debe mostrar el resultado de aplicar sobre el campo PRECIO 
#un descuento de un 7%. 
#El formato del nuevo campo para debe aparecer con 2 lugares 
#decimales.
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO,
    ROUND((productos.PRECIO - (productos.PRECIO * 0.07)), 2) 
    AS DESCUENTO_7
FROM
    productos