#Realizar una consulta que muestre el nombre del artículo, sección,
#pero y un nuevo campo que muestre el precio con el iva incluido
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO,
    ROUND((productos.PRECIO * 1.21), 2) AS PRECIO_CON_IVA
FROM
    productos
#
#Estas consultas son llamadas también consultas de campo nuevo 
#calculado, porque se crea un campo nuevo que no existe en la tabla
#para ello simplemente se coloca la expresión matemática y un alias
#Podemos utilizar la función ROUND e indicar en su segundo 
#parámetro la precisión decimal