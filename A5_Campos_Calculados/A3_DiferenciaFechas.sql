#Realizar una consulta que muestre el nombre del artículo, sección,
#fecha de la sección de deportes y un campo nuevo que muestre la
#diferencia en dias entre los artículos desde su entrega hasta
#la fecha actual, este ultimo también debe mostrarse
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO,
    productos.FECHA,
    DATE_FORMAT(NOW(), "%Y-%m-%d") AS DIA_ACTUAL,
    DATEDIFF(DATE_FORMAT(NOW(), "%Y-%m-%d"), productos.FECHA) 
    AS DIFERENCIA_DIAS
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES"
#
#Para realizar estas operaciones utilizamos la función NOW
#Que retorna la fecha y hora actual y la función DATEDIFF
#Que retorna la diferencia en dias entre dos fechas
#La segunda fecha se resta a la primera
#Para dar formato a la fecha se usa la función DATE_FORMAT y se le
#pasan dos parámetros, la fecha y el formato precedido de un %
#Para el año se usa la 'Y', para el mes la 'm' y para el dia la 'd'