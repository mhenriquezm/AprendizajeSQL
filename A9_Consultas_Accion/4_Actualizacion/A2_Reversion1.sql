#Revertir la consulta anterior
UPDATE
    productos
SET
    productos.PRECIO = productos.PRECIO - 10
WHERE
    productos.SECCIÓN = "DEPORTES"
#
#No existe forma de revertir una consulta de acción, pero en el 
#caso de ser una consulta de actualización se puede crear una query
#inversa para revertir los cambios, esto no funciona con las query
#de eliminación, por lo tanto es una práctica no recomendada