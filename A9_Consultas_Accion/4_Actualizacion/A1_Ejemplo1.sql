#Incrementarle 10$ al precio de todos los artículos de deportes
UPDATE
    productos
SET
    productos.PRECIO = productos.PRECIO + 10
WHERE
    productos.SECCIÓN = "DEPORTES"
#
#Para realizar actualizaciones utilizamos la cláusula UPDATE
#El nombre de la tabla, a continuación la cláusula SET seguida
#del campo a actualizar, en este tipo de consultas se debe 
#tener mucho cuidado al usar correctamente los criterios