#Eliminar los clientes que no han hecho pedidos
DELETE DISTINCTROW
	clientes.*,
    pedidos.CÓDIGOCLIENTE
FROM
    clientes
LEFT JOIN pedidos ON clientes.CÓDIGOCLIENTE = pedidos.CÓDIGOCLIENTE
WHERE
	pedidos.CÓDIGOCLIENTE IS NULL
#
#En este caso necesitamos utilizar el predicado DISTINCTROW debido
#a que realizamos una consulta de tipo LEFT JOIN que puede devolver
#registros duplicados y para ello se usa el predicado mencionado