#Eliminar todos los clientes de Madrid
DELETE
FROM
    clientes
WHERE
    clientes.POBLACIÓN = "MADRID"
#
#A la hora de utilizar el comando DELETE se debe
#tener mucho cuidado de utilizar correctamente los
#criterios con la cláusula WHERE debido a que estas
#consultas no tienen reverso y si se tiene integridad
#referencial eliminamos todos los registros relacionados