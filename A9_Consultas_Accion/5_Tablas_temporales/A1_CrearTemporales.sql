#Crear una tabla temportal con los clientes de Madrid
CREATE TABLE clientes_madrid SELECT
    *
FROM
    clientes
WHERE
    clientes.POBLACIÓN = "MADRID"
#
#Los comandos DML permiten crear tablas a partir de tablas ya 
#existentes, esto también se conoce como crear tablas temporales.
#Cada gestor de BBDD tiene su sintaxis para ejecutar estas acciones
#Access y SQLServer utilizan las instrucciones SELECT INTO, MySQL
#no soporta estas instrucciones y se debe usar CREATE TABLE y luego
#la query que retorna los resultados a almacenar en la tabla 
#temporal