#Realizar una consulta que actualice los precios de la tabla 
#ARTÍCULOS DE DEPORTE. La actualización consiste en calcular el 
#IVA (21%) y mostrar en ese campo como resultado el precio con 
#el IVA incluido. Ejecutar la consulta.
UPDATE
    articulos_de_deportes
SET
    PRECIO = PRECIO * 1.21