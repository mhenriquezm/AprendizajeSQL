#Realizar una consulta que actualice el campo DESCUENTO de la tabla
#PEDIDOS_PAGADOS_CON TARJETA. La actualización consiste poner a un 
#5% los descuentos que se muestran inferiores a esta cifra. 
#Ejecutar la consulta.
UPDATE
    pedidos_pagados_con_tarjeta
SET
    pedidos_pagados_con_tarjeta.DESCUENTO = 0.05
WHERE
    pedidos_pagados_con_tarjeta.DESCUENTO < 0.05