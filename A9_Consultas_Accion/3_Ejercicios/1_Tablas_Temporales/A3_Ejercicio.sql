#Realizar una consulta de acción de creación de tabla a partir de 
#la tabla PEDIDOS, utilizando todos los campos de la tabla, pero 
#sólo los registros que tengan registrada la forma de pago TARJETA.
#El nuevo objeto – tabla lo nombramos con el texto 
#PEDIDOS_PAGADOS_CON_TARJETA. Ejecutamos la consulta.
CREATE TABLE pedidos_pagados_con_tarjeta SELECT
    *
FROM
    pedidos
WHERE
    pedidos.FORMADEPAGO = "TARJETA"