#Realizar una consulta de acción de creación de tabla a partir de 
#la tabla CLIENTES, utilizando todos los campos de la tabla, pero 
#únicamente los registros que sean de la población Madrid. 
#El nuevo objeto lo nombramos con el texto CLIENTES_DE_MADRID. 
#Ejecutamos la consulta.
CREATE TABLE clientes_de_madrid SELECT
    *
FROM
    clientes
WHERE
    clientes.POBLACIÓN = "MADRID"