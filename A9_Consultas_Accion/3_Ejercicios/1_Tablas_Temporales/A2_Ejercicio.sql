#Realizar una consulta de acción de creación de tabla a partir de 
#la tabla PRODUCTOS, utilizando todos los campos de la tabla, pero 
#sólo los registros que sean de la sección DEPORTES. 
#El nuevo objeto – tabla lo nombramos con el texto 
#ARTÍCULOS_DE_DEPORTES. Ejecutamos la consulta.
CREATE TABLE articulos_de_deportes SELECT
    *
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTIVA"