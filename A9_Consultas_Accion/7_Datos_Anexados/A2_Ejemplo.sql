#Anexar registros con campos específicos
INSERT INTO clientes(
    clientes.CÓDIGOCLIENTE,
    clientes.EMPRESA,
    clientes.POBLACIÓN,
    clientes.TELÉFONO
)
SELECT
    clientes_de_madrid.CÓDIGOCLIENTE,
    clientes_de_madrid.EMPRESA,
    clientes_de_madrid.POBLACIÓN,
    clientes_de_madrid.TELÉFONO
FROM
    clientes_de_madrid
#
#En este tipo de consultas se debe tener precaución con los campos
#clave y campos indexados