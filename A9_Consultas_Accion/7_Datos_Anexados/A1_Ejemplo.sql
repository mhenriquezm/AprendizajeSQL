#La consulta de datos anexados permite incluir registros de una 
#tabla en otra
INSERT INTO clientes
SELECT
    *
FROM
    clientes_de_madrid
#
#Para ingresar todos los campos no se coloca nada, para agregar 
#campos por separado se incluye uno a uno entre paréntesis, luego 
#la consulta pertinente, pero esos datos anexados no tienen nada 
#que ver respecto a los registros relacionados