#Utilizando el predicado DISTINCT delante de un campo evitamos la
#redundancia, es decir, si está repetido se mostrará una vez
SELECT DISTINCT
    clientes.EMPRESA
FROM
    clientes
INNER JOIN pedidos ON clientes.CÓDIGOCLIENTE = pedidos.CÓDIGOCLIENTE
#
#Se deben considerar al momento de eliminar
#DISTINCTROW impide la repetición de un registro completo