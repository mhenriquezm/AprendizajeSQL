#Se puede colocar varias filas
TRANSFORM
    SUM(productos.PRECIO) AS TOTAL
SELECT 
    productos.NOMBREARTÍCULO,
    productos.PAÍSDEORIGEN
FROM 
    productos
GROUP BY
    productos.NOMBREARTÍCULO,
    productos.PAÍSDEORIGEN
PIVOT 
    productos.SECCIÓN
#
#En el campo PIVOT siempre se debe colocar un campo