#Realizar una consulta en la que se pueda ver el nombre del 
#artículo, la sección y el precio de cada registro de productos
TRANSFORM 
    SUM(PRECIO) AS TOTAL
SELECT 
    NOMBREARTÍCULO
FROM
    productos
GROUP BY
    NOMBREARTÍCULO
PIVOT
    SECCIÓN
#
#El primer paso es tener claro como es la estructura de una tabla 
#dinámica de Excel
#Todo comienza con el comando TRANSFORM que indica la 
#zona / campo totales (campo de cálculo)
#Luego campo que irá en la zona / campo filas cláusula GROUP BY
#Luego que campo que irá en la zona / campo columnas comando PIVOT