#Rediseñar el ejemplo invirtiendo filas en columnas
TRANSFORM 
    SUM(PRECIO) AS TOTAL
SELECT 
    SECCIÓN
FROM
    productos
GROUP BY
    SECCIÓN
PIVOT
    NOMBREARTÍCULO
    