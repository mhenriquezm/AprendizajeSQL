#Para hacer una referencia cruzada de varias tablas hacemos una 
#consulta de selección normal donde participen dichas tablas.
SELECT 
    clientes.EMPRESA, clientes.POBLACIÓN, pedidos.FORMADEPAGO
FROM
    clientes
INNER JOIN 
    pedidos ON clientes.CÓDIGOCLIENTE = pedidos.CÓDIGOCLIENTE
#
#Almacenamos la consulta previa y posteriormente realizamos el cruce