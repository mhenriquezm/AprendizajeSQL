-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2019 a las 14:10:21
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `curso_sql`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosnuevos`
--

CREATE TABLE `productosnuevos` (
  `CÓDIGOARTÍCULO` varchar(4) DEFAULT NULL,
  `SECCIÓN` varchar(18) DEFAULT NULL,
  `NOMBREARTÍCULO` varchar(18) DEFAULT NULL,
  `PRECIO` double(10,2) DEFAULT NULL,
  `FECHA` date DEFAULT NULL,
  `IMPORTADO` varchar(9) DEFAULT NULL,
  `PAÍSDEORIGEN` varchar(9) DEFAULT NULL,
  `FOTO` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productosnuevos`
--

INSERT INTO `productosnuevos` (`CÓDIGOARTÍCULO`, `SECCIÓN`, `NOMBREARTÍCULO`, `PRECIO`, `FECHA`, `IMPORTADO`, `PAÍSDEORIGEN`, `FOTO`) VALUES
('AR50', 'ALTA COSTURA', 'TRAJE CABALLERO', 1284.58, '2002-03-11', 'VERDADERO', 'ITALIA', NULL),
('AR51', 'DEPORTES DE RIESGO', 'RAQUETA TENIS', 1093.47, '2000-03-20', 'VERDADERO', 'USA', NULL),
('AR52', 'DEPORTES DE RIESGO', 'MANCUERNAS', 1060.00, '2000-09-13', 'VERDADERO', 'USA', NULL),
('AR53', 'ALTA COSTURA', 'SERRUCHO', 1030.20, '2001-03-23', 'VERDADERO', 'FRANCIA', NULL),
('AR54', 'ALTA COSTURA', 'PANTALÓN SEÑORA', 1174.23, '2000-01-10', 'VERDADERO', 'MARRUECOS', NULL),
('AR55', 'ALTA COSTURA', 'CAMISA CABALLERO', 1067.13, '2002-08-11', 'FALSO', 'ESPAÑA', NULL),
('AR56', 'DEPORTES DE RIESGO', 'PISTOLA OLÍMPICA', 1046.63, '2001-02-02', 'VERDADERO', 'SUECIA', NULL),
('AR57', 'ALTA COSTURA', 'BLUSA SRA.', 1101.06, '2000-03-18', 'VERDADERO', 'CHINA', NULL),
('AR58', 'ALTA COSTURA', 'CAZADORA PIEL', 1522.69, '2001-07-10', 'VERDADERO', 'ITALIA', NULL),
('AR59', 'DEPORTES DE RIESGO', 'BALÓN RUGBY', 1111.64, '2000-11-11', 'VERDADERO', 'USA', NULL),
('AR60', 'DEPORTES DE RIESGO', 'BALÓN BALONCESTO', 1075.27, '2001-06-25', 'VERDADERO', 'JAPÓN', NULL),
('AR61', 'ALTA COSTURA', 'ABRIGO CABALLERO', 1500.00, '2002-04-05', 'VERDADERO', 'ITALIA', NULL),
('AR62', 'DEPORTES DE RIESGO', 'BALÓN FÚTBOL', 1043.91, '2002-07-04', 'FALSO', 'ESPAÑA', NULL),
('AR63', 'ALTA COSTURA', 'ABRIGO SRA', 1360.07, '2001-05-03', 'VERDADERO', 'MARRUECOS', NULL),
('AR64', 'DEPORTES DE RIESGO', 'CRONÓMETRO', 1439.18, '2002-01-03', 'VERDADERO', 'USA', NULL),
('AR65', 'ALTA COSTURA', 'CINTURÓN DE PIEL', 1004.33, '2002-05-12', 'FALSO', 'ESPAÑA', NULL),
('AR66', 'DEPORTES DE RIESGO', 'CAÑA DE PESCA', 1270.00, '2000-02-14', 'VERDADERO', 'USA', NULL),
('AR67', 'DEPORTES DE RIESGO', 'BOTA ALPINISMO', 1144.00, '2002-05-05', 'FALSO', 'ESPAÑA', NULL),
('AR68', 'DEPORTES DE RIESGO', 'PALAS DE PING PONG', 1021.60, '2002-02-02', 'FALSO', 'ESPAÑA', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
