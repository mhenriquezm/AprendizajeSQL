#Realizar una consulta que muestre todos los artículos de deportes 
#o a deportes de riesgo
SELECT
    *
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES"
UNION
SELECT
    *
FROM
    productosnuevos
WHERE
    productosnuevos.SECCIÓN = "DEPORTES DE RIESGO"
#
#Para realizar este tipo de consultas simplemente planteamos una
#consulta sobre la primera tabla, a continuación el operador UNION
#y la consulta sobre la segunda tabla. En Access no se pueden 
#realizar este tipo de consultas de tablas que tengan campos tipo
#objeto ole