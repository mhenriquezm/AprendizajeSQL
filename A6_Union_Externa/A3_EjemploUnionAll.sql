SELECT
    *
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES"
UNION ALL
SELECT
    *
FROM
    productosnuevos
#
#La diferencia entre UNION y UNION ALL es que si existen registros
#con información identica en ambas tablas union muestra solo el 
#registro de la primera tabla, en cambio union all muestra todos
#los registros siempre