#Realizar una consulta que muestre los artículos de la tabla 
#productos cuyo precio sea superior a 500 y los artículos de la 
#tabla productosnuevos cuya sección sea alta costura
SELECT
    *
FROM
    productos
WHERE
    productos.PRECIO > 500
UNION
SELECT
    *
FROM
    productosnuevos
WHERE
    productosnuevos.SECCIÓN = "ALTA COSTURA"
#
#Para seleccionar un numero determinado de campos a mostrar en 
#este tipo de consultas ambas tablas deben mostrar los mismos campos