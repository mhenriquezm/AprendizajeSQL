#Mostrar todos los artículos de cerámica y deportes
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO
FROM
    productos
WHERE
    productos.SECCIÓN = "CERÁMICA" OR productos.SECCIÓN = "DEPORTES"

#Un producto no puede pertenecer a cerámica y deportes a la vez
#Es por ello que la consulta se hace de cerámica o de deportes
#Al usar el operador OR o el operador AND hay que repetir el campo