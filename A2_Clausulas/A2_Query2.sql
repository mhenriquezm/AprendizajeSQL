#Query para ver todos los productos de la sección de cerámica

#Comado SELECT + Cláusula FROM + Cláusula WHERE
#SELECT: mustra -> *: todos -> FROM: tabla -> WHERE: criterio
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO
FROM
    productos
WHERE
    productos.SECCIÓN = "CERÁMICA"