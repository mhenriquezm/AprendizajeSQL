#Query para ver todos los productos de la sección de cerámica

#Comado SELECT + Cláusula FROM + Cláusula WHERE
#SELECT: mustra -> *: todos -> FROM: tabla -> WHERE: criterio
SELECT * FROM productos WHERE productos.SECCIÓN = "CERÁMICA"

#Los valores de tipo cadena de texto siempre van entre comillas