#Realizar una consulta que muestre los artículos españoles 
#de la sección Deportes o aquellos cuyo precio sea superior a 350 € 
#independientemente de cual sea su sección o país de origen.
SELECT
    *
FROM
    productos
WHERE
    (
        productos.PAÍSDEORIGEN = "ESPAÑA" 
        AND productos.SECCIÓN = "DEPORTES"
    ) 
    OR productos.PRECIO > 350