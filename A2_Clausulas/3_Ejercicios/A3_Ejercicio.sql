#Realizar una consulta que muestre los productos de la sección 
#Deportes cuyo precio esté entre 100 y 200 €. 
#En la consulta solo se mostrarán los campos: 
#Nombre de artículo y Precio.
#
#Solución 1 usando operadores de comparación
SELECT
    productos.NOMBREARTÍCULO,
    productos.PRECIO
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES" 
    AND (productos.PRECIO >= 100 AND productos.PRECIO <= 200);
#
#Solución 2 con el operador BETWEEN
SELECT
    productos.NOMBREARTÍCULO,
    productos.PRECIO
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES" 
    AND (productos.PRECIO BETWEEN 100 AND 200)