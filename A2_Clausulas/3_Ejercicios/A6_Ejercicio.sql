#Realizar una consulta que muestre los productos cuya fecha esté 
#entre 1/05/2001 y 15/12/2001. 
#En la consulta solo se visualizarán los campos Nombre de artículo, 
#Sección y Fecha.
#
#Solución 1: utilizando el operador BETWEEN
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.FECHA
FROM
    productos
WHERE
    productos.FECHA BETWEEN "2001-05-01" AND "2001-12-15";
#
#Solución 2: utilizando operadores de comparación
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.FECHA
FROM
    productos
WHERE
    productos.FECHA >= "2001-05-01" 
    AND productos.FECHA <= "2001-12-15"