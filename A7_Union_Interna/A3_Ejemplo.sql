#Mostrar los clientes de Madrid que no han hecho pedidos
SELECT
    clientes.CÓDIGOCLIENTE,
    clientes.POBLACIÓN,
    clientes.DIRECCIÓN,
    pedidos.NÚMERODEPEDIDO,
    pedidos.CÓDIGOCLIENTE,
    pedidos.FORMADEPAGO
FROM
    clientes
LEFT JOIN pedidos ON clientes.CÓDIGOCLIENTE = pedidos.CÓDIGOCLIENTE
WHERE
    clientes.POBLACIÓN = "MADRID"
    AND pedidos.CÓDIGOCLIENTE IS NULL
ORDER BY
    clientes.CÓDIGOCLIENTE
#
#Para especificar que un campo es nulo se usa el criterio IS NULL