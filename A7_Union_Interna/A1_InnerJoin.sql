#Realizar una consulta que muestre los clientes de Madrid que han 
#hecho pedidos
SELECT
    *
FROM
    clientes
INNER JOIN pedidos ON clientes.CÓDIGOCLIENTE = pedidos.CÓDIGOCLIENTE
WHERE
    clientes.POBLACIÓN = "MADRID"
ORDER BY
	clientes.CÓDIGOCLIENTE