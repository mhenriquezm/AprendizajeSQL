#Realizar una consulta que muestre todos los clientes de Madrid 
#que han hecho pedidos y los que no han hecho pedidos
SELECT
    *
FROM
    clientes
LEFT JOIN pedidos ON clientes.CÓDIGOCLIENTE = pedidos.CÓDIGOCLIENTE
WHERE
    clientes.POBLACIÓN = "MADRID"
ORDER BY
	clientes.CÓDIGOCLIENTE