#Eliminar el campo fecha_baja y crearlo como tipo varchar
ALTER TABLE
    clientes_de_madrid
DROP COLUMN
    FECHA_BAJA;
ALTER TABLE
    clientes_de_madrid ADD COLUMN(FECHA_BAJA VARCHAR(10))
#
#Se pueden hacer varias consultas separadas por ;