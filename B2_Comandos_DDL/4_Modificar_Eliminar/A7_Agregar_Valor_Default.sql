#Agregar un campo llamado lugar_nacimiento con el valor por 
#defecto "DESCONOCIDO"
ALTER TABLE
    tabla_pruebas ADD COLUMN lugar_nacimiento VARCHAR(255);
ALTER TABLE
    tabla_pruebas
ALTER COLUMN
    lugar_nacimiento
SET
    DEFAULT "desconocido"