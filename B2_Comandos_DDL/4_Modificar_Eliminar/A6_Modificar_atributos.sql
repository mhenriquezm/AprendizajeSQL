#Cambiar el tipo de dato de fecha_baja de varchar a date
ALTER TABLE
    clientes_de_madrid CHANGE FECHA_BAJA FECHA_BAJA DATE
#
#Para hacer este tipo de operaciones luego de ALTER TABLE
#Usamos la cláusula CHANGE seguido del campo a alterar y 
#nuevamente dicho campo y el nuevo tipo de dato, en otros gestores
#se usa ALTER dos veces