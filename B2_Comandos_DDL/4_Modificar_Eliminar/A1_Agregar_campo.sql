#Agregar un campo nuevo llamado fecha_baja para registrar el dia 
#de eliminación de un cliente
ALTER TABLE
    clientes_de_madrid ADD COLUMN(FECHA_BAJA DATE)
#
#Luego de especificar ALTER TABLE y la tabla a modificar se
#Especifica que acción se realizará, si se va a agregar
#Un nuevo campo se usa la cláusula ADD COLUMN y a continuación
#El campo y su tipo