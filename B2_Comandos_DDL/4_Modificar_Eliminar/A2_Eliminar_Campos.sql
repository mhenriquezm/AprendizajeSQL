#Eliminar los campos de la tabla de pruebas que no sean id y nombre
ALTER TABLE
    tabla_pruebas
DROP COLUMN
    fecha_nacimiento,
DROP COLUMN
    apellido,
DROP COLUMN
    edad,
DROP COLUMN
    carnet
#
#Para eliminar varios campos se debe utilizar el comando 
#ALTER TABLE #Luego DROP COLUMN y el campo, seguido de la misma 
#expresión tantas veces como se necesite