#Crear una tabla especificando campos y tipos
CREATE TABLE tabla_pruebas(
    nombre VARCHAR(20),
    apellido VARCHAR(20),
    edad TINYINT,
    fecha_nacimiento DATE,
    carnet BOOLEAN
)