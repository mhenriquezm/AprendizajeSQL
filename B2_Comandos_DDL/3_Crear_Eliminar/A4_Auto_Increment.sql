#Crear una tabla con un campo ID autoincrementable
CREATE TABLE tabla_pruebas(
    id INT AUTO_INCREMENT,
    nombre VARCHAR(20),
    apellido VARCHAR(20),
    edad TINYINT,
    fecha_nacimiento DATE,
    carnet BOOLEAN,
    PRIMARY KEY (id)
)
#
#Un campo de tipo autoincrement debe ser obligatoriamente 
#clave primaria