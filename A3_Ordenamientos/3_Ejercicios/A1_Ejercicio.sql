#Realizar una consulta que muestre aquellos productos españoles 
#cuya fecha esté comprendida entre febrero y mayo del 2002. 
#Ordenar los resultados por el campo Nombre artículo 
#descendentemente.
#
#Solución 1: operador BETWEEN
SELECT
    *
FROM
    productos
WHERE
    productos.PAÍSDEORIGEN = "ESPAÑA" AND(
        productos.FECHA BETWEEN "2002-02-01" AND "2002-05-31"
    )
ORDER BY
    productos.NOMBREARTÍCULO
DESC;
#
#Solución 2: operadores de comparación
SELECT
    *
FROM
    productos
WHERE
    productos.PAÍSDEORIGEN = "ESPAÑA" AND(
        productos.FECHA >= "2002-02-01" 
        AND productos.FECHA <= "2002-05-31"
    )
ORDER BY
    productos.NOMBREARTÍCULO
DESC