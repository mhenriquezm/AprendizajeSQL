#Realizar una consulta que muestre los campos Empresa, Dirección
#y Población de la tabla Clientes. Ordenar la consulta por el campo
#Población ascendentemente y por Dirección descendentemente.
SELECT
    clientes.EMPRESA,
    clientes.DIRECCIÓN,
    clientes.POBLACIÓN
FROM
    clientes
ORDER BY
    clientes.POBLACIÓN ASC,
    clientes.DIRECCIÓN
DESC