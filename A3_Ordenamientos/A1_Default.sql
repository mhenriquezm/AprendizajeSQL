#Mostrar los artículos de deportes y cerámica ordenados por sección
SELECT
    *
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES" OR productos.SECCIÓN = "CERÁMICA"
ORDER BY
    productos.SECCIÓN

#Por defecto el ordenamiento que da el SGBD es de menor a mayor
#indiferentemente si un campo de texto o un campo numérico