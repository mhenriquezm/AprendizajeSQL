#Mostrar los artículos de deportes y cerámica ordenados por sección
SELECT
    *
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES" OR productos.SECCIÓN = "CERÁMICA"
ORDER BY
    productos.SECCIÓN ASC

#Para ordenar ascendentemente se utiliza el operador ASC