#Mostrar los artículos de deportes y cerámica ordenados por sección y precio
SELECT
    *
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES" OR productos.SECCIÓN = "CERÁMICA"
ORDER BY
    productos.SECCIÓN ASC, productos.PRECIO DESC
#
#No se puede ordenar por varios campos a la vez, pero si se pueden 
#establecer parios criterios o campos de ordenado, es decir, 
#ordenar por un campo y luego por otro. En este caso ordenamos 
#por sección y luego una vez ordenados por sección de ascendentemente
# se vuelven a ordenar por precio descendentemente