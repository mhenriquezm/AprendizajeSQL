#Crear un trigger que permita registrar en la tabla 
#registro_articulo la auditoria necesaria luego de insertar un 
#nuevo artículo en la tabla de productos
CREATE TRIGGER productos_ai AFTER INSERT ON
    productos FOR EACH ROW
INSERT INTO registro_articulo(
    codigo_articulo,
    nombre_articulo,
    precio,
    creacion
)
VALUES(
    NEW.CÓDIGOARTÍCULO,
    NEW.NOMBREARTÍCULO,
    NEW.PRECIO,
    NOW())
#
#Creamos el trigger con el comando create trigger y la convención 
#para nombrarlos es: nombre de la tabla a la que se asocia, seguida
# de un piso, luego cuando se ejecuta y la acción a ejecutar.
#Crear el trigger, nombrarlo, indicar antes o después y que acción
#en que tabla, si se ejecuta por cada acción o al finalizar el lote
#Acción a ejecutar. NEW y OLD hacen referencia a los estados del
#campo luego o antes del evento