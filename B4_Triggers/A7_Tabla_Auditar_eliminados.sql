#Crear una tabla para auditar los registros eliminados
CREATE TABLE auditoria_eliminados(
    c_art VARCHAR(5),
    nombre VARCHAR(255),
    seccion VARCHAR(50),
    precio DOUBLE(10, 2),
    pais_origen VARCHAR(50)
)