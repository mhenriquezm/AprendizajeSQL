#Crear una tabla para auditar las actualizaciones
CREATE TABLE auditoria_actualizaciones(
    codigo_articulo_anterior VARCHAR(5),
    nombre_articulo_anterior VARCHAR(255),
    seccion_articulo_anterior VARCHAR(50),
    precio_anterior DOUBLE(10, 2),
    importado_anterior VARCHAR(15),
    pais_origen_anterior VARCHAR(50),
    fecha_anterior DATE,
    codigo_articulo_nuevo VARCHAR(5),
    nombre_articulo_nuevo VARCHAR(255),
    seccion_articulo_nuevo VARCHAR(50),
    precio_nuevo DOUBLE(10, 2),
    importado_nuevo VARCHAR(15),
    pais_origen_nuevo VARCHAR(50),
    fecha_nuevo DATE,
    usuario VARCHAR(255),
    fecha_modificado DATE
)