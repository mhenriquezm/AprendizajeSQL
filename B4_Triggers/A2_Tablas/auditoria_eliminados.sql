-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2019 a las 12:55:12
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `curso_sql_triggers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_eliminados`
--

CREATE TABLE `auditoria_eliminados` (
  `c_art` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seccion` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL,
  `pais_origen` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_modificado` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `auditoria_eliminados`
--

INSERT INTO `auditoria_eliminados` (`c_art`, `nombre`, `seccion`, `precio`, `pais_origen`, `usuario`, `fecha_modificado`) VALUES
('AR75', 'Pantalón', NULL, 50.00, 'Venezuela', NULL, NULL),
('AR41', 'PALAS DE PING PONG', 'DEPORTES', 21.60, 'ESPAÑA', 'root@localhost', '2019-11-21'),
('AR75', 'BOLA TENIS', 'DEPORTES', 50.00, 'ESPAÑA', 'root@localhost', '2019-11-21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
