-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2019 a las 12:54:54
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `curso_sql_triggers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_articulo`
--

CREATE TABLE `registro_articulo` (
  `codigo_articulo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre_articulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL,
  `creacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `registro_articulo`
--

INSERT INTO `registro_articulo` (`codigo_articulo`, `nombre_articulo`, `precio`, `creacion`) VALUES
('AR75', 'Pantalón', 50.00, '2019-11-21 06:24:44'),
('AR75', 'BOLA TENIS', 50.00, '2019-11-21 07:53:53');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
