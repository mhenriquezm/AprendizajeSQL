#Agregar al trigger los campos usuario y fecha de modificacion
DROP TRIGGER IF EXISTS
    productos_ad;
CREATE TRIGGER productos_ad AFTER DELETE
ON
    productos FOR EACH ROW
INSERT INTO auditoria_eliminados(
    c_art,
    nombre,
    seccion,
    precio,
    pais_origen,
    usuario,
    fecha_modificado
)
VALUES(
    OLD.CÓDIGOARTÍCULO,
    OLD.NOMBREARTÍCULO,
    OLD.SECCIÓN,
    OLD.PRECIO,
    OLD.PAÍSDEORIGEN,
    CURRENT_USER(), NOW())
#
#Para modificar un trigger consiste primero en eliminarlo y 
#volverlo a crear, ambas instrucciones se pueden hacer en la misma
#sentencia