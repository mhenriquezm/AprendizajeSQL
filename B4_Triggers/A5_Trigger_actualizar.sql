#Crear un trigger que tome todos los valores antes de actualizar 
#y los nuevos, tambien el usuario y el momento de actualización
CREATE TRIGGER productos_bu BEFORE UPDATE
ON productos FOR EACH ROW
INSERT INTO auditoria_actualizaciones(
    codigo_articulo_anterior,
    nombre_articulo_anterior,
    seccion_articulo_anterior,
    precio_anterior,
    importado_anterior,
    pais_origen_anterior,
    fecha_anterior,
    codigo_articulo_nuevo,
    nombre_articulo_nuevo,
    seccion_articulo_nuevo,
    precio_nuevo,
    importado_nuevo,
    pais_origen_nuevo,
    fecha_nuevo,
    usuario,
    fecha_modificado
)
VALUES(
    OLD.CÓDIGOARTÍCULO,
    OLD.NOMBREARTÍCULO,
    OLD.SECCIÓN,
    OLD.PRECIO,
    OLD.IMPORTADO,
    OLD.PAÍSDEORIGEN,
    OLD.FECHA,
    NEW.CÓDIGOARTÍCULO,
    NEW.NOMBREARTÍCULO,
    NEW.SECCIÓN,
    NEW.PRECIO,
    NEW.IMPORTADO,
    NEW.PAÍSDEORIGEN,
    NEW.FECHA,
    CURRENT_USER(), NOW())
#
#Para capturar el usuario que realizo la consulta se usa la
#función CURRENT_USER y el instante de modificación con NOW,
#como ambos campos no son propios de productos no usamos new ni old