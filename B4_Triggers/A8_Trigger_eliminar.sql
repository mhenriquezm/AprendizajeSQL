#Crear un trigger para auditar la eliminación de productos
CREATE TRIGGER productos_ad AFTER DELETE
ON
    productos FOR EACH ROW
INSERT INTO auditoria_eliminados(
    c_art,
    nombre,
    seccion,
    precio,
    pais_origen
)
VALUES(
    OLD.CÓDIGOARTÍCULO,
    OLD.NOMBREARTÍCULO,
    OLD.SECCIÓN,
    OLD.PRECIO,
    OLD.PAÍSDEORIGEN
)