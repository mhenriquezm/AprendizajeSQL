#Tabla para auditar el ingreso de nuevos artículos
CREATE TABLE registro_articulo(
    codigo_articulo VARCHAR(25),
    nombre_articulo VARCHAR(255),
    precio DOUBLE(10, 2),
    creacion DATETIME
)