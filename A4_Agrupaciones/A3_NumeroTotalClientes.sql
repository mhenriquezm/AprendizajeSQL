#Mostrar el número de clientes por población
SELECT
    clientes.POBLACIÓN,
    COUNT(clientes.CÓDIGOCLIENTE) AS TOTAL
FROM
    clientes
GROUP BY
    clientes.POBLACIÓN
#
#Consideraciones sobre la función de agregado COUNT
#No cuenta registros vacíos o nulos, lo más común al usarla es
#Contar sobre un campo clave