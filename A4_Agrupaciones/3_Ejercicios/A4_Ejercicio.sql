#Realizar una consulta de agrupación que muestre la media del 
#precio de todas las secciones menos de juguetería. 
#En la consulta deberán aparecer los campos Sección y Media 
#por sección.
SELECT
    productos.SECCIÓN,
    CAST(
        AVG(productos.PRECIO) AS DECIMAL(38, 2)
    ) AS MEDIA_POR_SECCION
FROM
    productos
GROUP BY
    productos.SECCIÓN
HAVING
    productos.SECCIÓN <> "JUGUETERÍA"