#Realizar una consulta de agrupación que muestre la media del precio 
#de los artículos de todas las secciones. 
#Mostrar en la consulta los campos sección y media por sección.
SELECT
    productos.SECCIÓN,
    CAST(
        AVG(productos.PRECIO) AS DECIMAL(38, 2)
    ) AS MEDIA
FROM
    productos
GROUP BY
    productos.SECCIÓN