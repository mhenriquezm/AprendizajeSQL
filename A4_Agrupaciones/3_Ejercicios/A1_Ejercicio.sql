#Realizar una consulta sobre la tabla Clientes que muestre los 
#campos Dirección, Teléfono y Población. 
#Este último debe aparecer en la consulta con el nombre de 
#Residencia. Los registros aparecerán ordenados descendentemente 
#por el campo Población.
SELECT
    clientes.DIRECCIÓN,
    clientes.TELÉFONO,
    clientes.POBLACIÓN AS RESIDENCIA
FROM
    clientes
ORDER BY
    RESIDENCIA
DESC