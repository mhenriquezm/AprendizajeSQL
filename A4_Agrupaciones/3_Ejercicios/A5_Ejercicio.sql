#Realizar Una consulta que muestre cuantos artículos hay de la 
#sección Deportes.
SELECT
    productos.SECCIÓN,
    COUNT(productos.CÓDIGOARTÍCULO) AS TOTAL
FROM
    productos
GROUP BY
    productos.SECCIÓN
HAVING
    productos.SECCIÓN = "DEPORTES"