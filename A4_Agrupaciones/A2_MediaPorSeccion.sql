#Mostrar la media de los artículos de deportes y confección
SELECT
    productos.SECCIÓN,
    CAST(AVG(productos.PRECIO) AS DECIMAL(38, 2)) AS MEDIA
FROM
    productos
GROUP BY
    productos.SECCIÓN
HAVING
    productos.SECCIÓN = "DEPORTES" 
    OR productos.SECCIÓN = "CONFECCIÓN"
ORDER BY 
    MEDIA
#
#Las condiciones para grupos se hacen con la cláusula HAVING
#AVG devuelve la media aritmética del grupo
#CAST permite redondear pasando el numero a 2 espacios decimales