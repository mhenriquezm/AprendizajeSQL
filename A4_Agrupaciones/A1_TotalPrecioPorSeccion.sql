#Mostrar cuanto suman todos los articulos por sección
SELECT
    productos.SECCIÓN,
    SUM(productos.PRECIO) AS TOTAL
FROM
    productos
GROUP BY
    productos.SECCIÓN
ORDER BY
    TOTAL
#
#Para realizar estas consultas se necesitan 2 campos
#Campo de agrupación y campo de cálculo, al campo de cálculo se le 
#aplica una función de agregado, pero dicha función retorna un 
#valor que no existe en la tabla, para ello se utilizan los alias,
#Se aplica el operador AS a continuación del campo de calculo y 
#luego el alias que le queremos dar en la consulta, esto porque 
#la cláusula ORDER BY busca el criterio en la consulta.
#En resumen, en este tipo de consultas se necesitan únicamente 
#2 campos