#Mostrar el artículo con el precio más elevado de la sección 
#de confección
SELECT
    productos.SECCIÓN,
    MAX(productos.PRECIO) AS ALTOCOSTO
FROM
    productos
GROUP BY
    productos.SECCIÓN
HAVING
    productos.SECCIÓN = "CONFECCIÓN"
#
#En las funciones de agregado siempre se debe agrupar