#Tenemos un grupo de usuarios que hacen constantes consultas en
#la tabla de productos y esas consultas o instrucciones sql hacen
#referencia a la sección de artículos.
CREATE VIEW seccion_deportes AS SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO
FROM
    productos
WHERE
    productos.SECCIÓN = "DEPORTES"
#
#Las vistas son una especie de acceso directo, un objeto vinculado
#a la tabla origen que optimiza recursos en cada consulta y se
#actualizan junto con la tabla origen