#Vista para mostrar los artículos de cerámica
CREATE VIEW seccion_ceramica AS SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PRECIO
FROM
    productos
WHERE
    productos.SECCIÓN = "CERÁMICA"
#
#Esto es muy parecido a las consultas de access