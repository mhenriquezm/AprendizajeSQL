#Modificar una vista
ALTER VIEW
    seccion_deportes AS
SELECT
    productos.NOMBREARTÍCULO,
    productos.SECCIÓN,
    productos.PAÍSDEORIGEN
FROM
    productos
WHERE
    productos.PAÍSDEORIGEN = "ESPAÑA"